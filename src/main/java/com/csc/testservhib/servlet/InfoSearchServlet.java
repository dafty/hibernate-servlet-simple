/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.csc.testservhib.servlet;

import com.csc.testservhib.dao.ContractDetailsDao;
import com.csc.testservhib.dao.ContractDetailsDaoImpl;
import com.csc.testservhib.model.ContractDetails;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wind
 */
public class InfoSearchServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2110154506674132222L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        throw new ServletException("unsupported GET");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String contractNumber = req.getParameter("contractNumber");
        ContractDetailsDao dao = new ContractDetailsDaoImpl();
        List list = dao.findContractDetails(contractNumber);
        
        if(!list.isEmpty()) {
        	ContractDetails contractDetails = (ContractDetails) list.get(0);
            req.setAttribute("contractDetails", contractDetails);
            List itemDetails = dao.findItemDetails(contractDetails.getId().getProductCode());
            req.setAttribute("items", itemDetails);
            
            RequestDispatcher dispatcher = req.getRequestDispatcher("/searchResult.jsp");
            dispatcher.forward(req, resp);
            return;
        }        
        
        req.setAttribute("error", "Can't find contract information");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/error.jsp");
        dispatcher.forward(req, resp);
    }
    
}
