package com.csc.testservhib.model;

import java.io.Serializable;

public class ContractItemDescription implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1547806954503386442L;

    private String contractNumber;
    private String contractStatus;
    private String contractOwner;
    private String branchDetails;
       
    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getContractOwner() {
        return contractOwner;
    }

    public void setContractOwner(String contractOwner) {
        this.contractOwner = contractOwner;
    }

    public String getBranchDetails() {
        return branchDetails;
    }

    public void setBranchDetails(String branchDetails) {
        this.branchDetails = branchDetails;
    }
}
