package com.csc.testservhib.model;

import java.io.Serializable;

public class ContractDetailsId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 584864529875897L;

    private ContractItemDescription contractItem;
    private String productCode;

    public ContractDetailsId() {
    }

    public ContractDetailsId(ContractItemDescription contractItem, String productCode) {
        this.contractItem = contractItem;
        this.productCode = productCode;
    }
 
    public ContractItemDescription getContractItem() {
		return contractItem;
	}
    
    public void setContractItem(ContractItemDescription contractItem) {
		this.contractItem = contractItem;
	}
    
    public String getProductCode() {
		return productCode;
	}
    
    public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ContractDetailsId
            && productCode != null 
            && contractItem != null) {
            
            ContractDetailsId that = (ContractDetailsId) obj;
            return (that.productCode.equals(productCode) && that.contractItem.equals(contractItem));
        }
        return super.equals(obj); 
    }

    @Override
    public int hashCode() {
        if(productCode != null && contractItem != null) {
            return productCode.hashCode() + contractItem.hashCode();
        }
        
        return super.hashCode();
    }
    
}
