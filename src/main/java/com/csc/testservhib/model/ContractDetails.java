package com.csc.testservhib.model;

import java.io.Serializable;
import java.util.Date;

public class ContractDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5098596082424543447L;

    private ContractDetailsId id;
    
    private String companyCode;
    private Date issueDate;
    private ContractItemDescription contractDescription;

    public ContractDetailsId getId() {
        return id;
    }

    public void setId(ContractDetailsId id) {
        this.id = id;
    }

    public ContractItemDescription getContractDescription() {
        return contractDescription;
    }

    public void setContractDescription(ContractItemDescription contractDescription) {
        this.contractDescription = contractDescription;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}
