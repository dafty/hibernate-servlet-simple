package com.csc.testservhib.model;

import java.io.Serializable;

public class ItemDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -94864261517653047L;

    private String productCode;
    private String ItemLabel;
    private String productDescription;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getItemLabel() {
        return ItemLabel;
    }

    public void setItemLabel(String itemLabel) {
        ItemLabel = itemLabel;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
