/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.csc.testservhib.dao;

import java.util.List;

/**
 *
 * @author wind
 */
public interface ContractDetailsDao {
    public List findContractDetails(String contractNumber) ;
    public List findItemDetails(String productCode) ;
}
