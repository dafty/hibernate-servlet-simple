package com.csc.testservhib.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

//Purpose: Class to take care of Hibernate session management
public class HibernateUtil {
	/**
	 * Declaring session factory.
	 */
	private static SessionFactory sessionFactory = null;

	/**
	 * Method used to get Default session.
	 * 
	 * @return Session
	 */
	static {

		try {
			/**
			 * read the hibernate hibernatecfg.xml and prepare hibernate to use.
			 */
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

		} catch (HibernateException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Get session object
	 * 
	 * @return session
	 */
	public Session getSession() {

		/**
		 * Declaring session.
		 */
		Session session = null;

		session = sessionFactory.openSession();

		return session;
	}

	/**
	 * Method used to close statement.
	 * 
	 * @param statement
	 */
	public void closeResource(Statement statement) {

		try {
			if (null != statement) {
				statement.close();
			}
		} catch (SQLException sqlException) {
		}
	}

	/**
	 * Method used to close statement.
	 * 
	 * @param statement
	 */
	public void closeResource(ResultSet resultSet) {

		try {
			if (null != resultSet) {
				resultSet.close();
			}
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}
	}

	/**
	 * Method to close session.
	 */
	public void closeSession(Session session) {

		try {
			if (null != session) {
				session.close();
			}
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}

	}

}
