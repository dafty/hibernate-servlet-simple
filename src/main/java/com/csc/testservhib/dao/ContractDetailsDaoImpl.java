/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.csc.testservhib.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.csc.testservhib.model.ContractDetails;
import com.csc.testservhib.model.ItemDetails;

/**
 *
 * @author wind
 */
public class ContractDetailsDaoImpl implements ContractDetailsDao {

    private HibernateUtil hibernateUtil;
    public ContractDetailsDaoImpl() {
        hibernateUtil = new HibernateUtil();
    }

    @Override
    public List findContractDetails(String contractNumber) {        
        Session session = hibernateUtil.getSession();
        Criteria c = session.createCriteria(ContractDetails.class);        
        c.add(Restrictions.like("id.contractItem.contractNumber", contractNumber));
        return c.list();
    }    
    
    @Override
    public List findItemDetails(String productCode) {
    	Session session = hibernateUtil.getSession();
        Criteria c = session.createCriteria(ItemDetails.class);        
        c.add(Restrictions.like("productCode", productCode));
        return c.list();
    }
}
