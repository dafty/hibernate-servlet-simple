<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>MTL Contract Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">	
    </head>
    <body>
        <div style="min-width: 500px;">
            <h1>MTL Contract Information</h1>
            <div>
                <div>
                    <span> Contract Number:</span> <%=information.getContractNumber()%>
                </div>
                <div>
                    <span> Contract Issue Status: </span> <%=information.getContractIssueStatus()%>				
                </div>
                <div>
                    <span> Contract Owner:</span> <%=information.getContractOwner()%>
                </div>		
                <div>
                    <span> Issue Date: </span> <%=information.getIssueDate()%>
                </div>							
                <div>
                    <span> Branch: </span> <%=information.getBranch()%>
                </div>
                <div>
                    <span> Product code: </span> <%=information.getProductCode()%>
                </div>			
            </div>
        </div>
    </body>
</html> 