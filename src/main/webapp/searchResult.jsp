<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="com.csc.testservhib.model.*" %>        
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<% 
	ContractDetails contractDetails = (ContractDetails) request.getAttribute("contractDetails");
	List items = (List) request.getAttribute("items");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>MTL Contract Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">	
    </head>
    <body>
        <h1>MTL Contract Information</h1>
        <div style="min-width: 500px; margin: 10px;">
            <div class="result">
                <div>
                    <span> Contract Number:</span> 
                    <%=contractDetails.getId().getContractItem().getContractNumber()%>
                </div>
				<div>
                    <span> Contract Owner:</span> 
                    <%=contractDetails.getId().getContractItem().getContractOwner()%>
                </div>                
                <div>
                    <span> Contract Status:</span> 
                    <%=contractDetails.getId().getContractItem().getContractStatus()%>
                </div>
                <div>
                    <span> Branch Detail:</span> 
                    <%=contractDetails.getId().getContractItem().getBranchDetails()%>
                </div>
                <div>
                    <span> Issue Date:</span> 
                    <%=contractDetails.getIssueDate()%>
                </div>
                <div>
                    <span> Product Label:</span>
                    <ul>                     
                    <% 
                    	for(Object i : items) {
                    %>
                    	<li><%=((ItemDetails) i).getItemLabel()%> : <%=((ItemDetails) i).getProductDescription()%></li>
                    <% 
                    	} 
                   	%>                    
                </div>
            </div>
        </div>
    </body>
</html>
