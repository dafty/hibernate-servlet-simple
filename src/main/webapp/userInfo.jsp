<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>MTL Contract Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">	
        <script>
            function submitForm() {
                var form1 = document.getElementById("form1");
                var contractNumberVal = document.getElementById("contractNumber").value;
                var contractIssueStatus = document.getElementById("contractIssueStatus").value;
                var contractOwner = document.getElementById("contractOwner").value;

                if (contractNumberVal.length != 8) {
                    alert("Contract Number's lenght doesn't match");
                    return false;
                }

                if (contractIssueStatus == '') {
                    alert("Contract Issue Status is required");
                    return false;
                }

                if (contractOwner == '') {
                    alert("Contract owner Status is required");
                    return false;
                }

                var branchesChecked = false;
                var branches = document.getElementsByName("branch");
                for (var i = 0; i < branches.length; i++) {
                    if (branches[i].checked) {
                        branchesChecked = true;
                    }
                }

                if (!branchesChecked) {
                    alert("Branch is required");
                    return false;
                }

                var productCodeValidation = false;
                var productCodes = document.getElementsByName("productCode");
                for (var i = 0; i < productCodes.length; i++) {
                    if (productCodes[i].checked) {
                        productCodeValidation = true;
                    }
                }

                if (!productCodeValidation) {
                    alert("Product code is required");
                    return false;
                }


                if (!confirm("Do you want to continue?")) {
                    return false;
                }

                form1.submit();
            }

            m_curContactVal = ""; // global

            function numberCheck(e) {
                if (e.charCode < 48 || e.charCode > 57) {
                    e.preventDefault();
                }
            }
        </script> 
    </head>
    <body>
        <div style="min-width: 500px;">
            <h1>MTL Contract Information</h1>
            <form method="POST" action="<%=request.getContextPath()%>/userInfoServlet" id="form1">
                <div>
                    <span>Contract Number:</span> 				
                    <input type="text" name="contractNumber" id="contractNumber" onkeypress="numberCheck(event);" maxlength="8"/>				
                </div>
                <div>
                    <span>
                        Contract Issue Status: 
                    </span>

                    <select name="contractIssueStatus" id="contractIssueStatus">
                        <option value="">--Select--</option>
                        <option value="Inforce">Inforce</option>
                        <option value="Invalid">Invalid</option>
                        <option value="Valid">Valid</option>
                    </select>			
                </div>
                <div>
                    <span>Contract Owner:</span> 				
                    <input type="text" name="contractOwner" id="contractOwner"/>				
                </div>			
                <div>
                    <span>
                        Branch:
                    </span>			
                    <ul style="list-style: none;">
                        <li><input type="radio" name="branch" value="Head Office" /> Head Office</li>
                        <li><input type="radio" name="branch" value="Thailand" /> Thailand</li>
                        <li><input type="radio" name="branch" value="Singapore Office" /> Singapore Office</li>
                    </ul>				
                </div>
                <div>
                    <span>
                        Product code:
                    </span>

                    <ul style="list-style: none;">
                        <li><input type="checkbox" name="productCode" value="MTL Unit RiskRegular" /> RUL </li>
                        <li><input type="checkbox" name="productCode" value="MFL Unit MFL" /> ULS </li>
                    </ul>				
                </div>
                <div>			
                    <input type="button" value="Submit" onclick="submitForm();"/>			
                </div>
            </form>
        </div>
    </body>
</html> 