<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>MTL Contract Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">	
    </head>
    <body>
        <h1>MTL Contract Information</h1>
        <div style="min-width: 500px; margin: 10px;">
            <div>
                <span style="color : red;"><c:out value="${error}"/></span>
            </div>
        </div>
    </body>
</html>	
