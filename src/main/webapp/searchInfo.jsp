<%@page contentType="text/html" pageEncoding="ISO-8859-1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>MTL Contract Information</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">	
    </head>
    <body>
    	<h1>MTL Contract Information</h1>
        <div style="min-width: 500px; margin: 10px;">
           
            <form method="POST" action="<%=request.getContextPath()%>/infoSearchServlet" id="form1">
                <div>
                    <span>
                        Contract Number:
                    </span>
                    
                    <input type="text" name="contractNumber" maxlength="8" size="20" />
                </div>
                <div>
                    <input type="submit" value="Search">
                </div>
            </form>
        </div>
    </body>
</html>
