package com.csc.testservhib.dao.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.csc.testservhib.dao.ContractDetailsDao;
import com.csc.testservhib.dao.ContractDetailsDaoImpl;
import com.csc.testservhib.model.ContractDetails;

import static org.junit.Assert.*;

public class ContractDetailsDaoTest {

	private ContractDetailsDao dao;
	
	@Before
	public void before() {
		dao = new ContractDetailsDaoImpl(); 
	}
	
    @Test
    public void testFindContractDetails() {
        List result = dao.findContractDetails("00000781");
        assertNotNull(result);
        assertEquals(1, result.size());        
        assertEquals(ContractDetails.class, result.get(0).getClass());
        
        ContractDetails details = (ContractDetails) result.get(0);        
        assertEquals("RAWIN", details.getId().getContractItem().getContractOwner());
    }
}
